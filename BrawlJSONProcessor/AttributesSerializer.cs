﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BrawlJSONProcessor
{

    // Custom Serializer
    public class AttributesSerializer : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            AttributesJSON attributesSet = value as AttributesJSON;

            writer.WriteStartObject();
            foreach (AttributeDetailJSON attr in attributesSet.Attributes)
            {
                if (attr.Name.Equals(""))
                {
                    attr.Name = String.Format("Unknown ({0})", attr.ID);
                }
                attr.Name = attr.Name.Trim();

                writer.WritePropertyName(attr.Name);
                serializer.Serialize(writer, attr.Value);
            }
            writer.WriteEndObject();

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(AttributesJSON).IsAssignableFrom(objectType);
        }

        public override bool CanRead
        {
            get
            {
                return false;
            }
        }
    }
}
