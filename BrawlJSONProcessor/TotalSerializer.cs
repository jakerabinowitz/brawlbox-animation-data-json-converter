﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BrawlJSONProcessor
{
    class TotalSerializer : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            TotalJSON attributesSet = value as TotalJSON;

            writer.WriteStartObject();
            foreach (CharacterJSON character in attributesSet.Characters)
            {
                writer.WritePropertyName(character.Name);
                writer.WriteStartObject();

                writer.WritePropertyName("Attributes");
                serializer.Serialize(writer, character.Attributes);
                writer.WritePropertyName("SubActions");
                serializer.Serialize(writer, character.SubActions);

                writer.WriteEndObject();

            }
            writer.WriteEndObject();

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(TotalJSON).IsAssignableFrom(objectType);
        }

        public override bool CanRead
        {
            get
            {
                return false;
            }
        }
    }
}
