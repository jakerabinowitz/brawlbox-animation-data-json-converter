﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BrawlJSONProcessor
{
    class SubactionSerializer : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            SubActionsJSON subactionsList = value as SubActionsJSON;

            writer.WriteStartObject();
            foreach (SubActionsDetailJSON subAct in subactionsList.Subactions)
            {
                subAct.Name = subAct.Name.Trim();

                writer.WritePropertyName(subAct.Name);
                writer.WriteStartObject();

                writer.WritePropertyName("Duration");
                serializer.Serialize(writer, subAct.Duration);
                writer.WritePropertyName("Hitboxes");
                serializer.Serialize(writer, subAct.Hitboxes);
                writer.WritePropertyName("DetailedHitboxData");
                serializer.Serialize(writer, subAct.DetailedHitboxData);
                writer.WritePropertyName("GrabBoxes");
                serializer.Serialize(writer, subAct.GrabBoxes);
                writer.WritePropertyName("DetailedCatchData");
                serializer.Serialize(writer, subAct.DetailedCatchData);
                writer.WritePropertyName("Armor");
                serializer.Serialize(writer, subAct.Armor);
                writer.WritePropertyName("ThrowApplied");
                serializer.Serialize(writer, subAct.ThrowApplied);
                writer.WritePropertyName("DetailedThrowData");
                serializer.Serialize(writer, subAct.DetailedThrowData);
                writer.WritePropertyName("Intangibility");
                serializer.Serialize(writer, subAct.Intangibility);
                writer.WritePropertyName("BoneModifiers");
                serializer.Serialize(writer, subAct.BoneModifiers);
                writer.WritePropertyName("IASA");
                serializer.Serialize(writer, subAct.IASA);
                writer.WritePropertyName("AutoCancelWindow");
                serializer.Serialize(writer, subAct.AutoCancel);

                writer.WriteEndObject();

            }
            writer.WriteEndObject();

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(AttributesJSON).IsAssignableFrom(objectType);
        }

        public override bool CanRead
        {
            get
            {
                return false;
            }
        }
    }
}
