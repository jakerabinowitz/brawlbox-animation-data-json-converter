﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BrawlJSONProcessor
{
    [JsonConverter(typeof(TotalSerializer))]
    public class TotalJSON
    {
        public List<CharacterJSON> Characters { get; set; }
    }

    public class CharacterJSON
    {
        public String Name { get; set; }
        public AttributesJSON Attributes { get; set; }
        public SubActionsJSON SubActions { get; set; }
    }

    [JsonConverter(typeof(AttributesSerializer))]
    public class AttributesJSON
    {
        public AttributeDetailJSON[] Attributes;
    }

    public class AttributeDetailJSON
    {
        public String ID { get; set; }
        public String Name { get; set; }
        public double Value { get; set; }
    }

    [JsonConverter(typeof(SubactionSerializer))]
    public class SubActionsJSON
    {
        public List<SubActionsDetailJSON> Subactions { get; set; }
    }

    public class SubActionsDetailJSON
    {
        public String Name { get; set; }
        public int Duration { get; set; }
        public int[][] Hitboxes { get; set; }
        public List<DetailedHitboxJSON> DetailedHitboxData { get; set; }
        public int[][] GrabBoxes { get; set; }
        public List<DetailedCatchJSON> DetailedCatchData { get; set; }
        public List<Armor> Armor { get; set; }
        public int ThrowApplied { get; set; }
        public DetailedThrowData DetailedThrowData { get; set; }
        public int[] Intangibility { get; set; }
        public List<BoneStatusJSON> BoneModifiers { get; set; }
        public int IASA { get; set; }
        public int[][] AutoCancel { get; set; }
    }

    public class DetailedHitboxJSON
    {
        public int StartFrame { get; set; }
        public int EndFrame { get; set; }
        public List<OffensiveCollisionsJSON> collisions { get; set; }
    }

    public class OffensiveCollisionsJSON
    {
        public int Id { get; set; }
        public String Bone { get; set; }
        public int Damage { get; set; }
        public int ShieldDamage { get; set; }
        public int Direction { get; set; }
        public int BaseKnockback { get; set; }
        public int WeightKnockback { get; set; }
        public int KnockbackGrowth { get; set; }
        public double Size { get; set; }
        public double ZOffset { get; set; }
        public double YOffset { get; set; }
        public double XOffset { get; set; }
        public double TripRate { get; set; }
        public double HitlagMultiplier { get; set; }
        public double SDIMultiplier { get; set; }
        public int Flags { get; set; }
    }

    public class DetailedCatchJSON
    {
        public int StartFrame { get; set; }
        public int EndFrame { get; set; }
        public List<CatchCollisionJSON> CatchCollisions { get; set; }
    }

    public class CatchCollisionJSON
    {
        public int ID { get; set; }
        public String Bone { get; set; }
        public double Size { get; set; }
        public double XOffset { get; set; }
        public double YOffset { get; set; }
        public double ZOffset { get; set; }
        public int Action { get; set; }
        public int Type { get; set; }
        public int Unknown { get; set; }
    }

    public class BoneStatusJSON
    {
        public String Bones { get; set; }
        public String Status { get; set; }
        public int[][] Frames { get; set; }
    }

    public class DetailedThrowData
    {
        public int ID { get; set; }
        public int Bone_maybe { get; set; }
        public int Damage { get; set; }
        public int Direction { get; set; }
        public int KnockbackGrowth { get; set; }
        public int WeightKnockback { get; set; }
        public int BaseKnockback { get; set; }
        public int Element { get; set; }
        public double UnknownA { get; set; }
        public double UnknownB { get; set; }
        public double UnknownC { get; set; }
        public int UnknownD { get; set; }
        public int SFX { get; set; }
        public int Direction_maybe { get; set; }
        public bool UnknownE { get; set; }
        public bool UnknownF { get; set; }
        public int UnknownG { get; set; }
    }

    public class Armor
    {
        public string ArmorType { get; set; }
        public int Tolerance { get; set; }
        public int[] Frames { get; set; }
    }
}
