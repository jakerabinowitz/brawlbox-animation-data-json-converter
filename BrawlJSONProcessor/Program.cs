﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace BrawlJSONProcessor
{
    class Program
    {
        private static Regex formatAttributesDetect = new Regex(@"\{'(\w*)(.*)':(.*)\}");
        private static String formatAttributesReplace = "{ID: '$1', Name: '$2', Value: $3}";

        private static Regex removeSpacesDetect = new Regex(@"(\w) (\w)");
        private static String removeSpacesReplace = "$1$2";

        private static Regex handleDetailedDataDetect = new Regex(@"\'(\w*)\: (.*?)\'");
        private static String handleDetailedDataReplace = "{$2}";

        private static Regex formatBoneDetect = new Regex(@"Bone:(\w*),");
        private static String formatBoneReplace = "Bone: '$1',";

        private static Regex formatOffsetDetect = new Regex(@", Offset:\((.*?), (.*?), (.*?)\)");
        private static String formatOffsetReplace = ", XOffset: $1, YOffset: $2, ZOffset: $3";

        private static Regex removeRABasicDamageDetect = new Regex(@"Damage:\wA-Basic\[(.*?)], ");
        private static Regex removeRAFloatDamageDetect = new Regex(@"Damage:\wA-Float\[(.*?)], ");

        [STAThread]
        static void Main(string[] args)
        {
            List<CharacterJSON> allCharacters = new List<CharacterJSON>();

            String rootDirectoryPath = PickDirectory();


            foreach (String characterFolderPath in Directory.GetDirectories(rootDirectoryPath))
            {
                CharacterJSON thisCharacterJSON = new CharacterJSON();
                thisCharacterJSON.Name = Path.GetFileName(characterFolderPath);

                // Attributes
                String attributesFilePath = characterFolderPath + "\\Attributes_Data.txt";
                if (File.Exists(attributesFilePath))
                {
                    String characterAttributesText = File.ReadAllText(attributesFilePath);
                    characterAttributesText = formatAttributesDetect.Replace(characterAttributesText, formatAttributesReplace);
                    AttributesJSON attrs = JsonConvert.DeserializeObject<AttributesJSON>("{" + characterAttributesText + "}");
                    thisCharacterJSON.Attributes = attrs;
                }
                // Subactions
                List<SubActionsDetailJSON> subActionsList = new List<SubActionsDetailJSON>();
                foreach (String subActionFolderPath in Directory.GetDirectories(characterFolderPath))
                {
                    String[] dataFilePaths = Directory.GetFiles(subActionFolderPath, "*_Data.txt");
                    if (dataFilePaths.Length != 1)
                    {
                        Console.WriteLine("{0} has the wrong number of Data files.  Is {1}, should be 1", subActionFolderPath, dataFilePaths.Length);
                    }
                    else
                    {
                        String fileText = File.ReadAllText(dataFilePaths[0]);
                        // TODO: Need to preprocess text so that things like "HitlagMultiplier=x1" work.
                        fileText = fileText.Replace("\t", "");
                        fileText = fileText.Replace("=x", "=");
                        fileText = fileText.Replace("=", ":");
                        fileText = fileText.Replace("%", "");
                        fileText = fileText.Replace("?", "_maybe");

                        fileText = removeSpacesDetect.Replace(fileText, removeSpacesReplace);

                        fileText = handleDetailedDataDetect.Replace(fileText, handleDetailedDataReplace);

                        fileText = formatBoneDetect.Replace(fileText, formatBoneReplace);

                        fileText = formatOffsetDetect.Replace(fileText, formatOffsetReplace);

                        fileText = removeRABasicDamageDetect.Replace(fileText, "");
                        fileText = removeRAFloatDamageDetect.Replace(fileText, "");

                        SubActionsDetailJSON subactionJSON = JsonConvert.DeserializeObject<SubActionsDetailJSON>("{" + fileText + "}");
                        subactionJSON.Name = Path.GetFileName(subActionFolderPath);
                        subActionsList.Add(subactionJSON);
                    }
                }
                SubActionsJSON subActionsSet = new SubActionsJSON();
                subActionsSet.Subactions = subActionsList;
                thisCharacterJSON.SubActions = subActionsSet;
                allCharacters.Add(thisCharacterJSON);
            }

            // Done parsing characters;

            TotalJSON totalJSON = new TotalJSON();
            totalJSON.Characters = allCharacters;

            String outputJSON = JsonConvert.SerializeObject(totalJSON, Formatting.Indented);

            File.WriteAllText(rootDirectoryPath + @"\FrameData.json", outputJSON);
        }


        public static String PickDirectory()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                return dialog.SelectedPath;
            }
            return @"";
        }
    }
}
